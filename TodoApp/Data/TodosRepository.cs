﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Enums;
using TodoApp.DataContracts.Requests;

namespace TodoApp.Data
{
    public class TodosRepository : ITodosRepository
    {
        private readonly AppDbContext _dbContext;
        private readonly UserManager<User> _userManager;

        public TodosRepository(AppDbContext appDbContext, UserManager<User> userManager)
        {
            _dbContext = appDbContext;
            _userManager = userManager;
        }
        
        public List<TodoItem> GetAll(GetAllTodosRequest request, int? id)
        {
            var query = _dbContext.TodoItems.AsQueryable();

            if (!string.IsNullOrEmpty(request.Query))
            {
                query = query.Where(x => x.Title.ToLowerInvariant().Contains(request.Query.ToLowerInvariant()));
            }

            if (request.Completed.HasValue)
            {
                query = query.Where(x => x.Completed == request.Completed.Value);
            }

            if (request.SortDirection.HasValue)
            {
                query = request.SortDirection.Value == SortDirection.Asc
                    ? query.OrderBy(x => x.Title)
                    : query.OrderByDescending(x => x.Title);
            }

            if (id.HasValue) // if the user is not an admin
            {
                query = query.Where(todoItem => todoItem.UserId == id);
            }

            return query.ToList();
        }

        public TodoItem GetById(int id)
        {
            return _dbContext.TodoItems.Single(x => x.Id == id);
        }

        public TodoItem Create(CreateTodoItemRequest request, int userId)
        {
            if (_dbContext.TodoItems.FirstOrDefault(x => x.Title.ToLowerInvariant() == request.Title.ToLowerInvariant()) != null)
            {
                throw new ArgumentException($@"Todo item with title ""{request.Title}"" already exists.");
            }
            
            var item = new TodoItem
            {   
                Title = request.Title,
                Completed = false,
                UserId = userId,
            };

            _dbContext.TodoItems.Add(item);

            _dbContext.SaveChanges();

            return item;
        }

        public void Update(int id, UpdateTodoItemRequest request, int? userId)
        {
            var item = _dbContext.TodoItems.Single(x => x.Id == id);

            if (userId.HasValue)
            {
                if(item.UserId != userId.Value)
                {
                    throw new ArgumentException("You can only update your own todo items");
                }
            }

            item.Title = request.Title;
            item.Completed = request.Completed.GetValueOrDefault();

            _dbContext.SaveChanges();
        }

        public void Delete(int id, int? userId)
        {
            var item = _dbContext.TodoItems.Single(x => x.Id == id);

            if (userId.HasValue)
            {
                if (item.UserId != userId.Value)
                {
                    throw new ArgumentException("You can only update your own todo items");
                }
            }

            _dbContext.TodoItems.Remove(item);

            _dbContext.SaveChanges();
        }
    }
}
