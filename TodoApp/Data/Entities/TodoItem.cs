﻿using System.Collections.Generic;

namespace TodoApp.Data.Entities
{
    public class TodoItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Completed { get; set; }
        
        public User User { get; set; } 
        public int? UserId { get; set; }
    }
}
