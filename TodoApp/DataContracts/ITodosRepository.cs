﻿using System;
using System.Collections.Generic;
using TodoApp.Data.Entities;
using TodoApp.DataContracts.Requests;

namespace TodoApp.DataContracts
{
    public interface ITodosRepository
    {
        List<TodoItem> GetAll(GetAllTodosRequest request, int? id);

        TodoItem GetById(int id);

        TodoItem Create(CreateTodoItemRequest request, int userId);

        void Update(int id, UpdateTodoItemRequest request, int? userId);

        void Delete(int id, int? userId);
    }
}