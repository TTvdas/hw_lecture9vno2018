const defaultState = {
    loading: true
};

export default (state = defaultState, action) => {
    switch (action.type) {
    case 'LOGOUT_SUCCESS':
        return {
            ...state,
            loading: false,
            userInfo: null
        };
    case 'LOGOUT_ERROR':
        return {
            ...state,
            loading: false
        };
    default:
        return state;
    }
};
