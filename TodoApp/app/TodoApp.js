import React from "react";
import TodoAppNavigation from './TodoAppNavigation';
import { connect } from 'react-redux';

class TodoApp extends React.Component {
    render () {
        const appStyle = {
            backgroundColor: this.props.appSettings.appColor
        };

        return (
            <div className="container">
                <div className="todo-app" style={appStyle}>
                    {this.props.children}
                    <TodoAppNavigation />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {appSettings: state.appSettings}
};

export default connect(mapStateToProps, null)(TodoApp)