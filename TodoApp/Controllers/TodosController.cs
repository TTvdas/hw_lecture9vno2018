﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Requests;
using TodoApp.DataContracts.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace TodoApp.Controllers
{
    [Route("api/todos")]
    public class TodosController : Controller
    {
        private readonly ITodosRepository todosRepository;
        private readonly UserManager<User> _userManager;

        public TodosController(ITodosRepository todosRepository, UserManager<User> userManager)
        {
            this.todosRepository = todosRepository;
            _userManager = userManager;
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetAll([FromQuery]GetAllTodosRequest request)
        {
            var userIdClaim = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.NameIdentifier);
            int? userId = Int32.Parse(userIdClaim.Value);

            if (User.IsInRole("Admin"))
            {
                userId = null;
            }

            return Ok(todosRepository.GetAll(request, userId));
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                return Ok(todosRepository.GetById(id));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult CreateItem([FromBody]CreateTodoItemRequest request)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    var userIdClaim = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.NameIdentifier);
                    var userId = Int32.Parse(userIdClaim.Value);

                    var item = todosRepository.Create(request, userId);
                    return CreatedAtAction(nameof(GetById), item);
                }
                catch(ArgumentException e)
                {
                    return StatusCode((int)HttpStatusCode.Conflict, new ErrorResponse { ErrorMessage = e.Message });
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [HttpPut("{id}")]
        public IActionResult UpdateItem(int id, [FromBody]UpdateTodoItemRequest request) {
            if(ModelState.IsValid)
            {
                try
                {
                    var userIdClaim = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.NameIdentifier);
                    int? userId = Int32.Parse(userIdClaim.Value);

                    if (User.IsInRole("Admin"))
                    {
                        userId = null;
                    }

                    todosRepository.Update(id, request, userId);

                    return NoContent();
                }
                catch(ArgumentException e)
                {
                    return StatusCode((int)HttpStatusCode.Conflict, new ErrorResponse { ErrorMessage = e.Message });
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult DeleteItem(int id)
        {
            try
            {
                var userIdClaim = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.NameIdentifier);
                int? userId = Int32.Parse(userIdClaim.Value);

                if (User.IsInRole("Admin"))
                {
                    userId = null;
                }

                todosRepository.Delete(id, userId);

                return NoContent();
            }
            catch (ArgumentException e)
            {
                return StatusCode((int)HttpStatusCode.Conflict, new ErrorResponse { ErrorMessage = e.Message });
            }
        }
    }
}
