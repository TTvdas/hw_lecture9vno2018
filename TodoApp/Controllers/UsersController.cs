﻿using Microsoft.AspNetCore.Mvc;
using TodoApp.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using TodoApp.Data;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace TodoApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        private readonly UserManager<User> _userManager;

        public UsersController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_userManager.Users.ToList());
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AssignAdminRole(int userId)
        {
            var user = _userManager.Users.ToList().FirstOrDefault(innerUser => innerUser.Id == userId);
            if (user == null)
            {
                return NotFound("No such user");
            }
            await _userManager.AddToRoleAsync(user, "Admin");
            return Ok("successfully assigned role");
        }
    }
}