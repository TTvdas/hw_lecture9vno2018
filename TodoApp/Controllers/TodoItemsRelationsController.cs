﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;
using TodoApp.Data;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Requests;
using TodoApp.DataContracts.Responses;

namespace TodoApp.Controllers
{
    [Route("api/todoItemsrelations")]
    public class TodoItemsRelationsController : Controller
    {
        public TodoItemsRelationsController()
        {
        }
        
        [HttpPost("assignTaskForUser")]
        public void AssignTaskForUser(int userId, [FromQuery] string title, [FromQuery] bool completed)
        {
            
            using (AppDbContext db = new AppDbContext())
            {
                //Create new todoitem
                var newTodoItem = new TodoItem();
                newTodoItem.Title = title;
                newTodoItem.Completed = completed;

                //Find user
                User user = db.Users.Find(userId);

                //Detached scenarion, when only one call to database is done
                //User user = new User();
                //user.Id = userId;

                //Foreign key scenario
                //newTodoItem.UserId = userId;
                //db.Add(newTodoItem);

                if (user.TodoItems == null)
                {
                    user.TodoItems = new List<TodoItem>();
                }

                //assign task to user
                user.TodoItems.Add(newTodoItem);

                db.SaveChanges();
            }
        }

        [HttpPut("reassignTask")]
        public void ReassignTask([FromQuery] int userId, [FromQuery] int taskId)
        {
            using (AppDbContext db = new AppDbContext())
            {
                //Find user
                User newUser = db.Users.Find(userId);

                //Find todoitem
                TodoItem todoItem = db.TodoItems.Single(t => t.Id == taskId);
                
                if (newUser.TodoItems == null)
                {
                    newUser.TodoItems = new List<TodoItem>();
                }
                //re-assign task to user
                newUser.TodoItems.Add(todoItem);
                db.SaveChanges();


                //--------------------------------------
                //fakes approach
                //var todoItem = new TodoItem {Id = taskId};
                //db.Attach(todoItem);

                //var user = new User {Id = userId};
                //db.Attach(user);

                //user.TodoItems.Add(todoItem);
                //db.SaveChanges();

                //--------------------------------------
                //Altering foreign key approach
                //var todoItem = new TodoItem { Id = taskId };
                //db.Attach(todoItem);

                //todoItem.UserId = userId;

                //db.SaveChanges();
            }
        }

        [HttpDelete("unassignTask")]
        public void UnassignTask([FromQuery] int userId, [FromQuery] int taskId)
        {
            using (AppDbContext db = new AppDbContext())
            {
                //Find user
                User newUser = db.Users.Find(userId);

                //Find todoitem
                TodoItem todoItem = db.TodoItems.Find(taskId);

                
                //delete task to user
                newUser.TodoItems.Remove(todoItem);
                
                db.SaveChanges();
            }
        }
    }
}
