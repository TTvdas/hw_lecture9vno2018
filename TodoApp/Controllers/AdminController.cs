﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Requests;

namespace TodoApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Admin")]
    public class AdminController : Controller
    {
        private readonly ITodosRepository todosRepository;
        private readonly UserManager<User> _userManager;

        public AdminController(ITodosRepository todosRepository, UserManager<User> userManager)
        {
            this.todosRepository = todosRepository;
            _userManager = userManager;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult GetAll([FromQuery]GetAllTodosRequest request)
        {
            return Ok(todosRepository.GetAll(request, null));
        }
    }
}